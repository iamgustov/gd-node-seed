var winston = require('winston'),
	ENV = process.env.NODE_ENV || 'development';

var getLogger = function(module) {
	var path = module.filename.split('/').slice(-2).join('/');

	return new winston.Logger({
		transports: [
			new winston.transports.Console({
				colorize: true,
				level: ENV == 'development' ? 'debug' : 'error',
				label: path
			})
		]
	});
};

module.exports = getLogger;
