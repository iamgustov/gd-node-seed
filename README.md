GD-SEED Project
====================

App structure:

* app - main application folder
	* assets - include your stylesheets, scripts and images here
	* routes - routing logic
	* views - contains project views
* libs - place your libs here
* config - application config
	* config.json - main config file
* app.js - starting point

Installation
================================

* clone & cd to this repo
* npm install
* bower install
* node app.js
* browse http://0.0.0.0:PORT (port can be found at config/config.json file)
