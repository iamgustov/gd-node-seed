var express = require('express'),
	path = require('path'),
	favicon = require('serve-favicon'),
	logger = require('./libs/logs')(module),
	cookieParser = require('cookie-parser'),
	bodyParser = require('body-parser'),
	routes = require('./app/routes/'),
	config = require('./config'),
	app = express(),
	ejs = require('ejs-locals');

app.set('port', config.get('port'));
app.set('views', path.join(__dirname, config.get('views:dir')));
app.set('view engine', config.get('views:engine'));
app.engine('ejs', ejs);
//app.use(favicon(__dirname + '/assets/favicon.ico'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, config.get('assets:dir'))));

// Routes
app.use('/', routes);

app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: app.get('env') === 'development' ? err : {}
    });

	logger.error(err.message);
});

// Server
var server = app.listen(app.get('port'), function () {
	logger.info('GD-SEED listening at http://%s:%s', server.address().address, server.address().port);
});

module.exports = app;
