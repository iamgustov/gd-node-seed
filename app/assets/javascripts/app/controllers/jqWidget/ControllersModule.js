'use strict';

(function(angular, global) {
    var controllersModule = angular.module('jqWidgetApp.controllers', []);
    global.jqWidgetcontrollersModule = controllersModule;
})(angular, window);