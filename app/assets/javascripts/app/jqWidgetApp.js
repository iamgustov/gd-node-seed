'use strict';

(function(angular, global) {
    var jqWidgetApp = angular.module('jqWidgetApp', ['jqWidgetApp.controllers', 'jqwidgets', 'gridApp.models']);
    global.jqWidgetApp = jqWidgetApp;
})(angular, window);