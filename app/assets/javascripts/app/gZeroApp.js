'use strict';

(function(angular, global) {
    var gZeroApp = angular.module('gZeroApp', ['gZeroApp.directives']);
    global.gZeroApp = gZeroApp;
})(angular, window);