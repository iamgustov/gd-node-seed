'use strict';

(function(angular, global) {
    var gridApp = angular.module('gridApp', ['ngGrid', 'gridApp.controllers', 'gridApp.models']);
    global.gridApp = gridApp;
})(angular, window);