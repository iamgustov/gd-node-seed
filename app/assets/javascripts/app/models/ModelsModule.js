'use strict';

(function(angular, global) {
    var modelsModule = angular.module('gridApp.models', []);
    global.modelsModule = modelsModule;
})(angular, window);