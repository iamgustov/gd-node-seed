'use strict';

(function (angular, module) {
    module.directive('gzeroSocialButtons', function () {
        return {
            restrict: 'E',
            template:
                '<div style="font-size: 25px;">' +
                '   <a href="x"><i class="fa fa-facebook"></i></a>' +
                    '<a href="y"><i class="fa fa-twitter"></i></a>' +
                '   <a href="z"><i class="fa fa-instagram"></i></a>' +
                '</div>'
        };
    });
})(angular, gZeroDirectivesModule);