'use strict';

(function(angular, global) {
    var gZeroDirectivesModule = angular.module('gZeroApp.directives', []);
    global.gZeroDirectivesModule = gZeroDirectivesModule;
})(angular, window);