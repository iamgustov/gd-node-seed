'use strict';

$(document).ready(function () {
	Faker.generateAccounts(20000, function(data) {
		$("#grid").kendoGrid({
			dataSource: {
				data: data,
				pageSize: 20
			},
			height: 543,
			scrollable: {
				virtual: true
			},
			pageable: {
				info: true,
				numeric: false,
				previousNext: false
			},
			columns: [
				{field: 'acctName', title: 'Account Name'},
				{field: 'acctNum', title: 'Account No.'},
				{field: 'desc', title: 'Description'},
				{field: 'dSym', title: 'Symbol/CUSIP'},
				{field: 'current', title: 'Quantity'},
				{field: 'costBasisGLPerc', title: 'Price'},
				{field: 'hasOpen', title: 'Avg. Invest. Per Share'},
				{field: 'plans', title: 'Current Value'},
				{field: 'prdType', title: 'Amount Invested'},
				{field: 'quan', title: 'Invest. GL'},
				{field: 'shortName', title: 'Est. Annual Income'}
			]
		});
	});
});
