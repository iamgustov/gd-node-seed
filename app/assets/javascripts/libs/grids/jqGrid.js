'use strict';

$(document).ready(function () {
	var $jqGrid = $('#jq-grid');

	Faker.generateAccounts(20000, function(data) {
		$jqGrid.jqGrid({
			data: data,
			datatype: "local",
			height: 250,
			width: 1000,
			colNames: ['Account Name', 'Account No.', 'Description', 'Symbol/CUSIP', 'Quantity', 'Price', 'Avg. Invest. Per Share',
				'Current Value', 'Amount Invested', 'Invest. GL', 'Est. Annual Income'],
			colModel: [
				{name: 'acctName', index: 'acctName'},
				{name: 'acctNum', index: 'acctNum', sorttype: "date"},
				{name: 'desc', index: 'desc'},
				{name: 'dSym', index: 'dSym', align: "right", sorttype: "float"},
				{name: 'current', index: 'current', align: "right", sorttype: "float"},
				{name: 'costBasisGLPerc', index: 'costBasisGLPerc', align: "right", sorttype: "float"},
				{name: 'hasOpen', index: 'hasOpen', sortable: false},
				{name: 'plans', index: 'plans', sortable: false},
				{name: 'prdType', index: 'prdType', sortable: false},
				{name: 'quan', index: 'quan', sortable: false},
				{name: 'shortName', index: 'shortName', sortable: false}
			],
			caption: 'JQGrid test',
			rowNum: 20,
			rowList : [20,30,50],
			scroll: 1,
			loadonce: true,
			gridview: true,
			pager: '#p-jq-grid',
			//grouping: true,
			//groupingView : {
			//	groupField : ['acctName'],
			//	groupColumnShow : [true],
			//	groupText : ['<b>{0}</b>'],
			//	groupCollapse : false,
			//	groupOrder: ['asc'],
			//	groupSummary : [true],
			//	groupDataSorted : true
			//},
			//footerrow: true,
			//userDataOnFooter: true
		});

		$jqGrid.jqGrid('navGrid','#p-jq-grid', {del:false, add:false, edit:false}, {}, {}, {}, {multipleSearch:true});
	});
});
