var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/tests/kendo', function(req, res, next) {
	res.render('tests/kendo', { title: 'Kendo' });
});

router.get('/tests/ng-grid', function(req, res, next) {
	res.render('tests/ngGrid', { title: 'ngGrid' });
});

router.get('/tests/jq-grid', function(req, res, next) {
	res.render('tests/jqGrid', { title: 'jqGrid' });
});

router.get('/tests/jq-widgets', function(req, res, next) {
	res.render('tests/jqWidgets', { title: 'jqWidgets' });
});

module.exports = router;
